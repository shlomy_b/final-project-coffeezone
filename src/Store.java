/**
 * Store is a class holds methods that aid the coffeZoneApp
 * class to simulate an online coffee shop. It aids by 
 * providing a mock store object that reflects the ingridents 
 * table in the database. Calculation for ingredients are
 * done here, then passed to the database.
 * 
 * 
 * @author Shlomo Bensimhon
 * @since	2020-12-01
 *
 */


import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.*;


public class Store {
	static Connection c1;
	private ArrayList<String> ingredeints;
	private ArrayList<String> productIngredeints;
	
	public Store(Connection passedC1)  throws SQLException{
		c1 = passedC1;
		getIngredients();
		getProductIngredients();
	}
	
	public ArrayList<String> returnProductList(){
		return this.productIngredeints;
	}
	
	public ArrayList<String> returnIngredeints(){
		return this.ingredeints;
	}
	
	
	/**
	* getIngredients is a method that retrieves information
	* about the stock of the ingredients table. It then stores
	* that information in this.ingredeints
	*
	*/
	public void getIngredients() throws SQLException {
		String query = "SELECT PRODUCT_NAME, INGREDIENT_NAME, STOCK FROM INGREDIENTS1\r\n" + 
				"JOIN RECIPES1 USING (INGREDIENT_ID)\r\n" + 
				"jOIN PRODUCTS1 USING (PRODUCT_ID)";
		PreparedStatement p1 = c1.prepareStatement(query);
		ResultSet returnSet = p1.executeQuery();
		
		ArrayList<String> ingredientStock = new ArrayList<String>();
		while(returnSet.next()) {
			ingredientStock.add(returnSet.getString("PRODUCT_NAME") + " : " + returnSet.getString("INGREDIENT_NAME") + " : " + returnSet.getString("STOCK"));
		}
		this.ingredeints = ingredientStock;
	}
	
	
	
	public void updateIngredients(String product, int stock, String ingredient) throws SQLException {

		/*
		String query = "SELECT INGREDIENT_NAME, STOCK FROM INGREDIENTS1";
		
		PreparedStatement p1 = c1.prepareStatement(query);
		ResultSet returnSet = p1.executeQuery();
		
		ArrayList<String> ingredientStock = new ArrayList<String>();
		
		while(returnSet.next()) {
			ingredientStock.add(returnSet.getString("INGREDIENT_NAME") + " : " + returnSet.getString("STOCK"));
		}

		*/
		for(String c : this.ingredeints) {
			if(c.contains(product)) {
				//System.out.println(Integer.parseInt(c.substring(c.lastIndexOf(" ")+1)) - stock);
				this.ingredeints.set(ingredeints.indexOf(c), product + " : " + ingredient + " : " + (Integer.parseInt(c.substring(c.lastIndexOf(" ")+1)) - stock));

			}
		}
		/*
			for(int i = 0; i < ingredientStock.size(); i++) {
				if(ingredientStock.get(i).contains(product)) {
					this.ingredeints.set(i, product + " : " + (Integer.parseInt(ingredientStock.get(i).substring(ingredientStock.get(i).lastIndexOf(" ")+1)) - stock));
				}
			}
		*/	
	}
	

	/**
	* getProductIngredients is a method that retrieves information
	* about the stock of a product in the ingredients table. It then stores
	* that information in this.productIngredeints
	*
	*/
	public void getProductIngredients() throws SQLException {
		String query = "SELECT PRODUCT_NAME, INGREDIENT_NAME, QUANTITY FROM INGREDIENTS1\r\n" + 
				"JOIN RECIPES1 USING (INGREDIENT_ID)\r\n" + 
				"JOIN PRODUCTS1 USING (PRODUCT_ID)";
		
		PreparedStatement p1 = c1.prepareStatement(query);
		ResultSet returnSet = p1.executeQuery();
		
		ArrayList<String> productIngredientStock = new ArrayList<String>();
		
		while(returnSet.next()) {
			productIngredientStock.add(returnSet.getString("PRODUCT_NAME") + " : " + returnSet.getString("INGREDIENT_NAME") + " : " + returnSet.getString("QUANTITY"));
		}
		this.productIngredeints = productIngredientStock;
	}
	
	public void updateProductIngredients(String productName, int stock) throws SQLException {

		/*
		String query = "SELECT PRODUCT_NAME, INGREDIENT_NAME, QUANTITY FROM INGREDIENTS1\r\n" + 
				"JOIN RECIPES1 USING (INGREDIENT_ID)\r\n" + 
				"JOIN PRODUCTS1 USING (PRODUCT_ID);";
		
		PreparedStatement p1 = c1.prepareStatement(query);
		ResultSet returnSet = p1.executeQuery();
		
		ArrayList<String> productIngredientStock = new ArrayList<String>();
		
		
		while(returnSet.next()) {
			productIngredientStock.add(returnSet.getString("INGREDIENT_NAME") + " : " + returnSet.getString("QUANTITY"));
		}

		*/
		for(String c : this.productIngredeints) {
			if(c.contains(productName)) {
				//System.out.println(Integer.parseInt(c.substring(c.lastIndexOf(" ")+1)) - stock);
				productIngredeints.set(ingredeints.indexOf(c), productName + " : " + (Integer.parseInt(c.substring(c.lastIndexOf(" ")+1)) - stock));
			}
		}
	}
}





/*
public static void checkOut(ArrayList<String> OrderList, double totalPrice, Store s1) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
	Scanner reader = new Scanner(System.in);
	int numberOfProducts = 0;
	//StringBuilder itemsOrdered = new StringBuilder(); // turn into string array 
	
	String[] productsOrdered = new String[OrderList.size()];
 	
	for(int i = 0; i < OrderList.size();i++){
		productsOrdered[i] = OrderList.get(i);
		}
	
	
	String query = "{SELECT COUNT(ORDER_ID) FROM ORDERS1;}";
	PreparedStatement p1 = c1.prepareStatement(query);
	ResultSet returnSet = p1.executeQuery();
	
	int order_id = 0;
	while(returnSet.next()) {
		order_id = Integer.parseInt(returnSet.getString("COUNT(ORDER_ID)"));
	}
	
	System.out.println(order_id);
	
	
	/*
	 * c1.setAutoCommit(false);
	 * 
	 * 
					 			
					 			
		INSERT INTO PRODUCTS_PER_ORDER (ORDER_ID, PRODUCT_ID)
		VALUES (order_id,  ); 
					 			

		statement.executeUpdate(�UPDATE COFFEES SET TOTAL = � + total + �WHERE COF_NAME = ESPRESSO�);

			c1.commit();

	 */
	
	
	
	/*
	
	System.out.println(" ");
	System.out.println("Your Order------------------------------------------------------");
	for(String s : OrderList) {
		System.out.println(s);
		numberOfProducts += (Integer.parseInt(s.substring(s.length() - 1, s.length())));
		itemsOrdered.append(s.substring(0, s.indexOf("||") - 1) + ",");

		// GET TOTAL NUMBER OF STOCK USED PER ORDER
	}		
	System.out.println("---------------------------------------------------Total: " + totalPrice);
	System.out.println(" ");
	System.out.println("Your Address For This Order Is: " + user_address);
	System.out.println("If Would You Like To Update The Delivery Address, Enter it below, If Not type No");
		String address = reader.nextLine();
		System.out.println(itemsOrdered);

		String query = "{CALL ADD_ORDER(?, ?, ?, ?, ?)}";
		CallableStatement callStatement = c1.prepareCall(query);
		
		callStatement.setInt(1, cust_id);
		callStatement.setString(2, address.toUpperCase());
		callStatement.setString(3, itemsOrdered.toString());
		callStatement.setInt(4, numberOfProducts);
		callStatement.registerOutParameter(5, Types.INTEGER);
		callStatement.execute();
		
		int result = callStatement.getInt(5);
		
		if(result == 0) {
			System.out.println("Order Was Not Successful");
			startMenu(s1);
			
		}else {
			System.out.println("Order Was Successful");
			System.out.println("Logging Off");
			updateStock(s1, itemsOrdered);
			user = false;
		}
	
	// update ingredients stock?
	
}


*/


























