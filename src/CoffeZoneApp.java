/**
 * CoffeZoneApp is a class holds many methods that can be 
 * used to simulate an online coffee shop. Using this class,
 * a User can register, login, and place orders.
 * 
 * 
 * @author Shlomo Bensimhon
 * @since	2020-12-01
 *
 */


import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.sql.*;
import java.util.*;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

public class CoffeZoneApp {
	static Connection c1 = getConnection();
	static boolean user = false;
	static int cust_id;
	static String user_address;

	public static void main(String[] args) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException{
		Scanner reader = new Scanner(System.in);
		
		startMenu();
	}
	
	/**
	* Connection Creates a connection to the database PDborad12c.
	*
	*/
	public static Connection getConnection() {
		Connection conn = null;
			try{
				// * Change this String to refer to your database. 
				String url = "jdbc:oracle:thin:@198.168.52.73:1522/pdborad12c.dawsoncollege.qc.ca"; //local URL
					conn = DriverManager.getConnection(url, "A1837702", "shlomo2000");
			}catch(SQLException e) {
				e.printStackTrace();
			}
			return conn; 		
	}
	
	/**
	* startMenu displays an interface for the user to help
	* them navigate the coffee shop menu.
	*
	* @param  S1	The Store object that holds information for for the coffee shop. 
	*/
	public static void startMenu() throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
		System.out.println("WELCOME TO COFFEEZONE");
		
		Scanner reader = new Scanner(System.in);
		
		System.out.println("1. Registration");
		System.out.println("2. Login");
		System.out.println("3. Place Order");
		int pick = reader.nextInt();
		
		if(pick == 1) {
			registration();
		}else if(pick == 2) {
			login();
		}else if(pick == 3) {
			order();
		}
	}
	
	/**
	* registration is the registration menu of the coffee shop
	* application. A user can register an account here. Once 
	* the information is collected, the information is added
	* to the database.
	*
	* @param  S1	The Store object that holds information for for the coffee shop. 
	*/
	public static void registration() throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
		Scanner reader = new Scanner(System.in);

		System.out.println("WELCOME TO REGISTRATION");
		System.out.println("Please Enter Your Full Name");
		String name = reader.nextLine();
		System.out.println("Please Enter Your Address");
		String address = reader.nextLine();
		System.out.println("Please Enter Your Email (OPTIONAL)");
		String email = reader.nextLine();
		System.out.println("Please Enter Your Phone Number (OPTIONAL)");
		String phoneNumber = reader.nextLine();
		
		
		if(name.equals("") || address.equals("")) {
			System.out.println("You Did Not Enter A Valid Name OR Address");
			registration();
		}
		
		// this validates the address, if its unique, it adds info to cust table
		String registQuery = "{CALL VALIDATE_REGISTRATION(?, ?, ?, ?, ?)}";
		CallableStatement callStatement = c1.prepareCall(registQuery);
		
		callStatement.setString(1, name.toUpperCase());
		callStatement.setString(2, address.toUpperCase());
		callStatement.setString(3, email.toUpperCase());
		callStatement.setString(4, phoneNumber.toUpperCase());
		callStatement.registerOutParameter(5, Types.INTEGER);
		callStatement.execute();
		int regisResult = callStatement.getInt(5);

		
		// 1 means its good, 0 means user already exists
		if(regisResult == 0){
			System.out.println("Cannot Create User, Conflicting Info"); // add niceness
			startMenu();
		}else {
			create_user();
			}
		}
	
	
	/**
	* create_user is part of the registration menu of the coffee shop
	* application. A user can register an username and password here.
	* Once the information is collected, the information is added
	* to the database.
	*
	* @param  S1	The Store object that holds information for for the coffee shop. 
	*/
	public static void create_user() throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
		Scanner reader = new Scanner(System.in);
		
		System.out.println("Please Enter A Username");
		String username = reader.nextLine();
		System.out.println("Please Enter A passWord");
		String password = reader.nextLine();
		System.out.println("Please Enter A Refferal_ID (OPTIONAL)");
		String refferal = reader.nextLine();
		
		
		/* 
		 * Didn't add salt and hash because it would require changes to user table and i don't have time to complete that.
		 * 
		SecureRandom random = new SecureRandom();
		String salt = new BigInteger(130, random).toString(32);
		String hash_algorithm = "lkjn3487$#%seff43sdf";
		
		PBEKeySpec spec = new PBEKeySpec((password + salt).toCharArray());
		SecretKeyFactory skf = SecretKeyFactory.getInstance(hash_algorithm);
		byte[] hashedKey = skf.generateSecret(spec).getEncoded();
		
		
		//setByte
		//String secretPassword = hash(password);
		
		*/
		String validateUser = "{CALL CREATE_USER(?, ?, ?, ?)}";
		CallableStatement callStatement2 = c1.prepareCall(validateUser);
		
		callStatement2.setString(1, username.toUpperCase());
		callStatement2.setString(2, password.toUpperCase());
		callStatement2.setString(3, refferal.toUpperCase());
		callStatement2.registerOutParameter(4, Types.INTEGER);
		callStatement2.execute();
		int userResult = callStatement2.getInt(4);

		if(userResult == 0){
			System.out.println("User Name is Taken");
			create_user();
		}else {
			System.out.println("User Created Succesfully");
			startMenu();
			}
		}

	/**
	* login is a method used to validate the users information 
	* in order to log into the coffee shop application. The 
	* user name and password are validated against the database.
	* If the log in is successful, the user_id and user_address 
	* are store in the application for later use.
	*
	* @param  S1	The Store object that holds information for for the coffee shop. 
	*/
	public static void login() throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
		Scanner reader = new Scanner(System.in);
		
		System.out.println("WELCOME TO LOGIN");
		System.out.println("Please Enter A Username");
		String username = reader.nextLine();
		System.out.println("Please Enter A passWord");
		String password = reader.nextLine();
		
		String query = "{CALL LOGIN(?, ?, ?, ?, ?)}";
		CallableStatement callStatement = c1.prepareCall(query);
		
		callStatement.setString(1, username.toUpperCase());
		callStatement.setString(2, password.toUpperCase());
		callStatement.registerOutParameter(3, Types.VARCHAR);
		callStatement.registerOutParameter(4, Types.INTEGER);
		callStatement.registerOutParameter(5, Types.INTEGER);
		callStatement.execute();
		
		int result = callStatement.getInt(5);
		
		if(result == 1) {
			System.out.println("Login Successful");
			cust_id = callStatement.getInt(4);
			user_address = callStatement.getString(3);
			user = true;
			startMenu();
		}else {
			System.out.println("The Username Or Password Combination Was Incorrect");
			System.out.println("Please Enter 0 To Try Again Or Any Other Number To go back");
			int loginFailed = reader.nextInt();
			
			if(loginFailed == 0) {
				login();
				user = false;
			}else {
				startMenu();
			}
		}
	}
	
	/**
	* order is a method used to display the product menu to all.
	* If a user is logged in, they have the option to place an 
	* order, if not, they can simply view the menu.
	*
	* @param  S1	The Store object that holds information for for the coffee shop. 
	*/
	public static void order() throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
		// need to check if items are available before checkout ** check Stock
		Scanner reader = new Scanner(System.in);
		
		ArrayList<String> productIdList = new ArrayList<String>();
		ArrayList<String> OrderList = new ArrayList<String>();
		List<Double> productPrices = new ArrayList<Double>();
		double totalPrice = 0;
		
		System.out.println("MENU:");

		String query = "SELECT * FROM PRODUCTS1";
		PreparedStatement p1 = c1.prepareStatement(query);
		ResultSet returnSet = p1.executeQuery();
		
		int counter = 0;
		while(returnSet.next()) {
			System.out.printf("ID: " + returnSet.getString("PRODUCT_ID") + returnSet.getString("PRODUCT_NAME") + " || PRICE: " + returnSet.getString("PRODUCT_PRICE"));
			System.out.println("");
			productPrices.add(Double.parseDouble(returnSet.getString("PRODUCT_PRICE")));
			productIdList.add(returnSet.getString("PRODUCT_NAME") + ":" + returnSet.getString("PRODUCT_ID"));
			counter++;
		}
		if(user) {
			System.out.println(" ");
			System.out.println("Please Enter The ID's of the Products You Would Like To Add To Your Cart (One At A Time)");
			System.out.println("Once All Your Items Have Been Added, Enter 0 to CheckOut Or -1 To Cancel And Return To Menu");
			System.out.println("========================================================================================" + "\n");
		
		int productId = 1;
		while(productId != 0) {
			System.out.print("Item Id: ");
			productId = reader.nextInt();
			
			if(productId == -1){
				System.out.println("Order Canceled" + "\n" + "Returning to Main Menu");
				startMenu();
			}else if(productId == 0){
				System.out.println("You Existed");
				checkOut(OrderList, totalPrice);
			}else if(productId > counter || productId < 0) {
					System.out.println("The Product ID You Entered Does Not Exist. Try Again");
			}else {
				System.out.print("Quantity: ");
				int quantity = reader.nextInt();
				OrderList.add(productIdList.get(productId - 1) + " || Quantity: " + quantity);
				totalPrice += (quantity * productPrices.get(productId - 1));
				System.out.println("Your Total Price Is: " + totalPrice);
				}
			}
		}else {
			System.out.println("Please Login To Place An Order");
			startMenu();
		}
	}
	
	/**
	* checkOut is a method that lets a logged in user place an
	* order. The user selects what they would like to order,
	* and the order is pushed to the database along with their
	* information.
	*
	* @param  S1	The Store object that holds information for for the coffee shop. 
	*/
	public static void checkOut(ArrayList<String> OrderList, double totalPrice) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException {
		Scanner reader = new Scanner(System.in);
		int numberOfProducts = 0;
		StringBuilder itemsOrdered = new StringBuilder(); // turn into string array 
		
		System.out.println(" ");
		System.out.println("Your Order------------------------------------------------------");
		for(String s : OrderList) {
			numberOfProducts += (Integer.parseInt(s.substring(s.length() - 1, s.length())));
			System.out.println(s.substring(0, s.indexOf(":")) + " Quantity: " + Integer.parseInt(s.substring(s.length() - 1, s.length())));
			itemsOrdered.append(s.substring(0, s.indexOf("||") - 1) + ",");
				}		
		System.out.println("Total Products: " + numberOfProducts);
		System.out.println("---------------------------------------------------Total: " + totalPrice);
		System.out.println(" ");
		System.out.println("Your Address For This Order Is: " + user_address);
		System.out.println("If Would You Like To Update The Delivery Address, Enter it below, If Not type No");
		String address = reader.nextLine();

			String query = "{CALL ADD_ORDER(?, ?, ?, ?, ?)}";
			CallableStatement callStatement = c1.prepareCall(query);
			
			callStatement.setInt(1, cust_id);
			callStatement.setString(2, address.toUpperCase());
			callStatement.setString(3, itemsOrdered.toString());
			callStatement.setInt(4, numberOfProducts);
			callStatement.registerOutParameter(5, Types.INTEGER);
			callStatement.execute();
			
			int result = callStatement.getInt(5);
			
			if(result == 0) {
				System.out.println("Order Was Not Successful");
				startMenu();
			}else {
				System.out.println("Order Was Successful");
				System.out.println("Placing Order");
				updateStock(OrderList);
				user = false;
			}
		}
	
	/**
	* updateStock is a method that updates the store object's
	* ingredient list. Once the list is updated in accordance to
	* what the user has ordered, the list is then pushed to the 
	* database. This method is called after every order.
	*
	* @param  S1	The Store object that holds information for for the coffee shop. 
	*/
	public static void updateStock(ArrayList<String>  itemsOrdered) throws SQLException, NoSuchAlgorithmException, InvalidKeySpecException  {		
		String[] productsOrdered = new String[itemsOrdered.size()];
	 	
		for(int i = 0; i < itemsOrdered.size();i++){
			productsOrdered[i] = itemsOrdered.get(i).substring(itemsOrdered.get(i).indexOf(":") + 1, itemsOrdered.get(i).indexOf("||"));
			}
		
		String query = "SELECT COUNT(ORDER_ID) FROM ORDERS1";
		PreparedStatement p1 = c1.prepareStatement(query);
		ResultSet returnSet = p1.executeQuery();
		
		int order_id = 0;
		while(returnSet.next()) {
			order_id = Integer.parseInt(returnSet.getString("COUNT(ORDER_ID)"));
		}
		
		//c1.setAutoCommit(false);	
		 for(int i = 0; i < productsOrdered.length; i++) {
			 String query2 = "INSERT INTO PRODUCTS_PER_ORDER1 (ORDER_ID, PRODUCT_ID)\r\n" + 
			 				"VALUES (" + order_id + "," + productsOrdered[i] +" )";
				PreparedStatement p2 = c1.prepareStatement(query2);
				ResultSet returnSet2 = p2.executeQuery();			 			
		 	}
		//c1.commit();
		 System.out.println("Order Was Placed" + "\n" + "Returning To Main Menu");
		 startMenu();
		 user = false;
	}
}	
